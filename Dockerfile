FROM gricad-registry.univ-grenoble-alpes.fr/osug/dc/shiny-base:4.0.2

# Install dependencies
RUN install.r \
      data.table \
      foreach \
      ggplot2 \
      plotly \
      ggnewscale \
      scales \
      ggthemes \
      reshape2 \
      && rm -rf /tmp/downloaded_packages

# Copy app files
COPY app.R COMP.hour.txt COMP.hour.bis.txt SitesOrchamp.csv /srv/shiny/
